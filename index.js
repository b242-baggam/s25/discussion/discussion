// JSON objects
/*
	Syntax:
		{
			// "propertyA": "valueA", // whole thing is considered as a strings
			// "propertyB": "valueB"
		}
}
*/

/*
	{
		"city": "Quezon city",
		"province": "Metro Manila",
		"country": "Philippines"
	}
*/

/*
	JSON Arrays

	"cities": [
		{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"},

		{"city": "Manila city", "province": "Metro Manila", "country": "Philippines"},

		{"city": "Makati city", "province": "Metro Manila", "country": "Philippines"}
	]
*/

// JSON Methods

// Converting Data into Stringified JSON

let batchesArr = [{ batchName: "Batch X"},{batchName: 'Batch Y'}];

console.log('Result from stringify method:');
console.log(JSON.stringify(batchesArr));

// Converting Data directly from an object

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log(data);
// when there are numbers, the stringify method won't convert the numbers.

// JSON are more frequently seen when gone to backend

// Using stringify method with variables
/*
	Syntax:
		JSON.stringify({
			propertyA: variableA,
			propertyB: variableB
		})
*/

// User details
let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city address belong to?")
};
let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);

// Converting Stringified JSON into JavaScript Objects
// For now, we need to understand that the objects can also be expressed in a manner like this

let batchesJSON = `[{"batchName": "Batch X"},{"batchName": "Batch Y"}]`;

console.log('Result from parse method; ');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{ "name": "John", "age": "31", "address": {"city": "Manila", "country": "Philippines"} }`;

console.log(JSON.parse(stringifiedObject));

// JSON used to transfer data from one place to another
// parse - JSON to JS
// stringify - JS to JSON